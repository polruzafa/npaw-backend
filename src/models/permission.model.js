import mongoose from 'mongoose'

const permissionSchema = mongoose.Schema({
  name: String,
  description: String
})

export default mongoose.model('Permission', permissionSchema)
