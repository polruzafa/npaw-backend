import mongoose from 'mongoose'

const accountSchema = mongoose.Schema({
  name: String,
  permissions: [{ type: mongoose.Schema.ObjectId, ref: 'Permission' }]
})

export default mongoose.model('Account', accountSchema)
