import AccountModel from './account.model'
import PermissionModel from './permission.model'

export default {
  AccountModel,
  PermissionModel
}
