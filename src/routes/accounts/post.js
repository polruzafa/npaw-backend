import Account from '../../models/account.model'
import _log from '../../aux/underlog'

export default (req, res) => {
  if (!req.body.account) {
    res.status(400).send('Expecting a nice JSON object, got shit >:(')
  } else {
    _log('[Account] POST: Someone posted a new account')

    let newAccount = new Account(req.body.account)

    newAccount.save().then(
      account => { res.status(201).send(account) },
      error => { res.status(500).send(error) }
    )
  }
}
