import Account from '../../models/account.model'
import _log from '../../aux/underlog'

export default (req, res) => {
  console.log(req.params)
  if (!req.params.id) {
    res.status(400).send('Expecting an id, received none >:(')
  } else {
    _log(`[Accounts] PUT: Someone requested to update id: ${req.params.id}`)

    if (!req.body.account) {
      res.status(400).send('Expecting a nice JSON object, got shit >:(')
    } else {
      Account.findOneAndUpdate({_id: req.params.id}, { $set: req.body.account }, {'new': true}).populate('permissions').exec((error, account) => {
        if (error) {
          res.status(500).send(`Error: ${error}`)
        }

        if (!account) {
          _log(`[Accounts] List is empty`)
          res.status(204).send(account)
        } else {
          res.send(account)
        }
      })
    }
  }
}
