import Account from '../../models/account.model'
import _log from '../../aux/underlog'

export default (req, res) => {
  if (req.params.id) {
    _log(`[Accounts] GET: Someone requested id: ${req.params.id}`)
    Account.findOne({_id: req.params.id}).populate('permissions').exec((error, account) => {
      if (error) {
        res.status(500).send(`Error: ${error}`)
      }

      if (!account) {
        _log(`[Accounts] List is empty`)
        res.status(204).send(account)
      } else {
        res.send(account)
      }
    })
  } else {
    _log(`[Accounts] GET: Someone requested all accounts`)

    Account.find().populate('permissions').exec((error, accounts) => {
      if (error) {
        res.status(500).send(`Error: ${error}`)
      }

      if (accounts.length === 0) {
        _log(`[Accounts] List is empty`)
        res.status(204).send(accounts)
      } else {
        res.send(accounts)
      }
    })
  }
}
