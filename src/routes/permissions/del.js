import Permission from '../../models/permission.model'
import _log from '../../aux/underlog'

export default (req, res) => {
  if (!req.params.id) {
    res.status(400).send('Expecting a id, received none >:(')
  } else {
    _log(`[Permissions] DEL: Someone requested to remove id ${req.params.id}`)

    Permission.remove({_id: req.params.id}, error => {
      if (error) {
        res.status(500).send(error)
      }
      res.status(200).send(req.params.id)
    })
  }
}
