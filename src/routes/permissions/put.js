import Permission from '../../models/permission.model'
import _log from '../../aux/underlog'

export default (req, res) => {
  if (!req.params.id) {
    res.status(400).send('Expecting an id, received none >:(')
  } else {
    _log(`[Permissions] PUT: Someone requested to update id: ${req.params.id}`)

    if (!req.body.permission) {
      res.status(400).send('Expecting a nice JSON object, got shit >:(')
    } else {
      Permission.findOneAndUpdate({_id: req.params.id}, { $set: req.body.permission }, {'new': true}, (error, permission) => {
        if (error) {
          res.status(500).send(`Error: ${error}`)
        }

        if (!permission) {
          _log(`[Permissions] List is empty`)
          res.status(204).send(permission)
        } else {
          res.send(permission)
        }
      })
    }
  }
}
