import hello from './hello.route'
import accounts from './accounts/routes'
import permissions from './permissions/routes'

export default {
  hello,
  accounts,
  permissions
}
