import _log from './aux/underlog'
import samples from './aux/samples'

export default () => {
  _log(`[Database] We're in the database: it's hacking time!`)

  // This function REMOVES every document from accounts and permissions collections
  // Remove it from production environment deploys: asumes mvertes/alpine-docker image is being used
  samples()
}
