import models from '../models/models'
import _log from './underlog'

export default () => {
  _log(`[Database] Let's add some sample data...`)

  models.PermissionModel.remove({}).then(
    success => {
      Promise.all([
        new models.PermissionModel({
          name: 'Read',
          description: 'Enables reading data from disk'
        }).save(),
        new models.PermissionModel({
          name: 'Write',
          description: 'Enables writing data to disk'
        }).save(),
        new models.PermissionModel({
          name: 'Execute',
          description: 'Enables executing binaries'
        }).save()
      ]).then(
        values => {
          models.AccountModel.remove({}).then(
            success => {
              Promise.all([
                new models.AccountModel({
                  name: 'Chuck Norris',
                  permissions: values.map(permission => permission._id)
                }).save(),
                new models.AccountModel({
                  name: 'Bruce Willis',
                  permissions: [values[0]._id]
                }).save(),
                new models.AccountModel({
                  name: 'Jackie Chan',
                  permissions: [values[0]._id, values[1]._id]
                }).save()
              ]).then(
                values => { _log(`[Database] ✔️ Done!`) },
                error => { console.log(error) }
              )
            },
            error => console.log(error)
          )
        },
        error => { console.log(error) }
      )
    },
    error => { console.log(error) }
  )
}
