## NPAW
# Backend

## Setup
```
git clone ..
cd npaw-backend
npm install
```

## Run
First time you'll need to initialize a database. 
Running `npm run database` will download the mvertes/alpine-mongo docker image and run it

```
npm start
```
It will start the mongo container created by `npm run database` and start the server on port `8088`